import hashlib
from datetime import datetime, timedelta

# ccf6d34d44b09aa8ee7948eee20aa8bf
hash = input("Code to find: ")
role = "admin"
hashArray = []

count = 0
printable = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
poss = 14776336.0
pourcent = 0
tStartList = datetime.now()

for i in printable:
    possibilities = ''
    for j in printable:
        for f in printable:
            for d in printable:
                possibilities= i+j+f+d
                hashArray.append(possibilities)

tEndList = datetime.now()
tList = tEndList - tStartList

print(f'Time for list creation of {poss} possibilities: {tList}.')

tStartCheck = datetime.now()

for item in hashArray:
    testHash = role+item
    testHash = hashlib.md5(testHash.encode())
    testHash = testHash.hexdigest()
    count +=1

    if testHash.upper() == hash.upper():
        pourcent = count*(100/poss)
        pourcent = ("%.3f" % pourcent)
        print(f'Code found in {count} tries : {item}')
        print(f'% of possibilites tried: {pourcent}')
        break

tEndCheck = datetime.now()
tCheck = tEndCheck - tStartCheck
tTotal = tList + tCheck
print(f'Time to find code in list: {tCheck}.')
print(f'Total time needed: {tTotal}.')