import sys, json, os

def jsonRW(line):
    try:
        json_input = json.loads(line)
        file = open("/Users/admin/Desktop/socket_server/key.json","r")
        try:
            json_file = json.loads(file.read())
            for key,val in json_input.items():
                if key in json_file: # exists in file, update
                    print("Updating '%s' from '%s' to '%s'" % (key,json_file[key],val))
                else:
                    print("Inserting '%s' : '%s'" % (key,val))
                json_file[key] = val
            # endfor
        except ValueError: # conf file is empty or isn't in JSON format
            json_file = json_input
        file.close()
        # now write
        file = open("/Users/admin/Desktop/socket_server/key.json","w")
        file.write(json.dumps(json_file))
        file.close()
        return 'JSON file modified.\nNew entry ? (CTRL-C to leave)\n'
    except ValueError:
        print("Error : invalid JSON syntax.")
        return 'Error : invalid JSON syntax.\nNew entry ? (CTRL-C to leave)\n'
    except KeyboardInterrupt:
        return 'Closing JSON file edit.\n'