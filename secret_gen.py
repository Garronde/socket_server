import string
import secrets

def secretGen():
    alphabet = string.ascii_letters + string.digits
    while True:
        secret = ''.join(secrets.choice(alphabet) for i in range(4))
        if (any(c.islower() for c in secret)
                and any(c.isupper() for c in secret)
                and sum(c.isdigit() for c in secret) >= 1):
            #print(secret)
            return secret
