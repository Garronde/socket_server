#!/usr/bin/env python3

import socket, base64, hashlib
from data_check import dataCheck
from token_gen import tokenGen
from json_modify import jsonRW


HOST = '192.168.1.20'  # Standard loopback interface address (localhost)
PORT = 10024      # Port to listen on (non-privileged ports are > 1023)


socket.socket(socket.AF_INET, socket.SOCK_STREAM).close()  # closing socket (to prevent "already in use")

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()        # return 2values: connexion socket (conn) and connexion adress (addr)
    with conn:
        print(f'Connected by {addr}')
        token = tokenGen("user")
        conn.sendall((f'Your token: {token}').encode())
        conn.sendall((f'\nEnter a token: ').encode())
        while True:
            data = conn.recv(1024)   # define buffersize
            data_ret = dataCheck(data) # retrieve value of dataCheck fonction (if ok = 'user ok')
            if data_ret == 'user ok':
                conn.sendall(data_ret.encode())
                break
        # endwhile
        conn.sendall((f'\nEnter value for JSON files: \n').encode())
        sock_in = conn.makefile()
        while True:
            conn.sendall(jsonRW(str(sock_in.readline())).encode())  # convert readline()'s entry in string because not using readlines()