import hashlib


def tokenGen(role):
    secret = "W3ak"
    group = 'group55'
    signature = role+secret
    signature = hashlib.md5(signature.encode())
    signature = signature.hexdigest()

    token = '.'.join([group, role, str(signature)])
    return token